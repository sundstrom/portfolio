package com.mygdx.entites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class BackgroundMusic implements Runnable {

	private Music backgroundMusic;

	public BackgroundMusic(){
		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/eyesunveiled.mp3"));
		backgroundMusic.setVolume(.5f);
	}

	@Override
	public void run() {
		backgroundMusic.play();
	}
	
	public void pause(){
		backgroundMusic.pause();
	}
	
	public void resume(){
		backgroundMusic.play();
	}


}
