package com.mygdx.entites;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactFilter;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Ball implements ContactFilter, ContactListener {

	private BodyDef bodyDef = new BodyDef();
	private FixtureDef fixtureDef = new FixtureDef();
	private Body ball;
	private Fixture ballFixture, endFixture;
	private Vector2 startCordinates;
	private World world;
	private Sprite ballSprite;
	private long start;
	private boolean finished = false;
	private Sound victory = Gdx.audio.newSound(Gdx.files.internal("sounds/victory.wav"));
	private Sound woodHit = Gdx.audio.newSound(Gdx.files.internal("sounds/woodhit.wav"));
	private Sound trampoline = Gdx.audio.newSound(Gdx.files.internal("sounds/spring.wav"));
	private Sound impulse = Gdx.audio.newSound(Gdx.files.internal("sounds/impulse.wav"));
	private LinkedList<Box> boxes;
	private String strVictory = " ";
	
	//Tanken �r att utveckla med denna s� den �r sann medans bollen har kontakt och p� s� s�tt kunna p�verka impulse eller force.
	private boolean boolContact = false;


	public Fixture getBallFixture(){
		return ballFixture;
	}

	public Ball (World world, Vector2 startCordinates, Fixture endFixture, LinkedList<Box> boxes){

		this.startCordinates = startCordinates;
		this.boxes = boxes;
		this.world = world;
		this.endFixture = endFixture;
		bodyDef.type = BodyType.DynamicBody;			//Type of body			//Following two lines are commented out due to that this shape has been attched to the box, and are the same body.
		bodyDef.position.set(startCordinates.x, startCordinates.y);  					//Starting position


		//Ball shape
		CircleShape ballShape = new CircleShape();
		ballShape.setRadius(1f);

		//Fixture definition, the physical properties
		fixtureDef.shape = ballShape;
		fixtureDef.density = 5f;  //In kg per m2.
		fixtureDef.friction = 1f;
		fixtureDef.restitution = 0.3f;

		ball = world.createBody(bodyDef);
		ballFixture = ball.createFixture(fixtureDef);

		ballSprite = new Sprite(new Texture("image/Moon.png"));
		ballSprite.setSize(3.5f, 3.5f);		//Scaling the sprite to be the same size as the box, needed since sprite uses pixels, and Box2d uses meters
		ballSprite.setOrigin(ballSprite.getWidth() / 2, ballSprite.getHeight() / 2); //Setting the origin to the middle of the sprite
		ball.setUserData(ballSprite);

		start = System.currentTimeMillis();
		world.setContactFilter(this);
		world.setContactListener(this);

		ballShape.dispose();

	}

	public void restart(){
		ball.setTransform(startCordinates.x, startCordinates.y, MathUtils.degRad * 0);
		ball.setAwake(false);
		ball.setAwake(true);
	}

	public void destroy(){
		world.destroyBody(ball);
	}

	public Body position(){
		return ball;
	}

	@Override
	public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {

		if(fixtureA == ballFixture && fixtureB == endFixture && !finished || fixtureA == endFixture && fixtureB == ballFixture && !finished){
			victory.play(.5f);
			finished = true;
			long end = System.currentTimeMillis();	
			//strVictory = " " + (double)(end-start) / 1000 + " seconds!";

		}
		return true;
	}

	@Override
	public void beginContact(Contact contact) {
		for(Box box: boxes){
			if (contact.getFixtureA() == box.getFixture() || contact.getFixtureB() == box.getFixture()){
				if (box.getChoice() == Box.NORMAL)
					woodHit.play(.5f);

				else if (box.getChoice() == Box.BOUNCY)
					trampoline.play();

				else if(box.getChoice() == Box.BOOSTJUMP){
					impulse.play(.7f);

				}
				else if (box.getChoice() == Box.SPEEDXPLUS){
					ball.applyLinearImpulse(ball.getLinearVelocity().x + 100  , ball.getLinearVelocity().y, ball.getPosition().x, ball.getPosition().y, true);
					impulse.play(.7f);	

				}
				else if (box.getChoice() == Box.SPEEDXMINUS){
					ball.applyLinearImpulse(ball.getLinearVelocity().x - 100 , ball.getLinearVelocity().y, ball.getPosition().x, ball.getPosition().y, true);
					impulse.play(.7f);	
				}
			}
		}

	}

	@Override
	public void endContact(Contact contact) {
		boolContact = false;
		for(Box box: boxes){
			if (contact.getFixtureA() == box.getFixture() || contact.getFixtureB() == box.getFixture()){
				if (box.getChoice() == 3){
					ball.applyLinearImpulse(ball.getLinearVelocity().x+ box.getFixture().getBody().getAngle() , ball.getLinearVelocity().y + 200 , ball.getPosition().x, ball.getPosition().y, true);
					System.out.println(box.getFixture().getBody().getAngle());
			}			
		}
	}

}

@Override
public void preSolve(Contact contact, Manifold oldManifold) {
	// TODO Auto-generated method stub

}

@Override
public void postSolve(Contact contact, ContactImpulse impulse) {
	// TODO Auto-generated method stub

}

public String message(){
	return strVictory;
}


}
