package com.mygdx.entites;

import java.util.ArrayList;
import java.util.Random;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Box {

	private BodyDef bodyDef = new BodyDef();
	private FixtureDef fixtureDef = new FixtureDef();
	private Body box;
	private World world;
	private Sprite boxSprite;
	private ArrayList<String> sprites = new ArrayList<String>();
	public static final int NORMAL = 1, BOUNCY = 2, BOOSTJUMP = 3, SPEEDXMINUS = 4, SPEEDXPLUS = 5;
	private int choice;
	private Fixture boxFixture;
	
	public Fixture getFixture(){
		return boxFixture;
	}

	public int getChoice(){
		return choice;
	}

	public Box (World world, Vector2 startCordinates, float halfWidth, float halfHeight, int angle, int boxType){
		
		this.world = world;
	
		choice = boxType;
		bodyDef.type = BodyType.StaticBody;			//Type of body			//Following two lines are commented out due to that this shape has been attached to the box, and are the same body.
		bodyDef.position.set(startCordinates.x, startCordinates.y);  
		bodyDef.angle = MathUtils.degRad * angle; //Starting position

		sprites.add("image/box4.jpg");
		sprites.add("image/box5.jpg");
		sprites.add("image/box6.jpg");
		sprites.add("image/box7.jpg");

		//Ball shape
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(halfWidth, halfHeight);

		//Fixture definition, the physical properties
		fixtureDef.shape = boxShape;
		fixtureDef.density = 5f;  //In kg per m2.
		fixtureDef.friction = 1f;

		if (boxType == BOUNCY){
			fixtureDef.restitution = 1f;
			boxSprite = new Sprite(new Texture("image/trampoline.gif"));
		}

		else if (boxType == NORMAL){
			fixtureDef.restitution = 0.3f;
			boxSprite = new Sprite(new Texture("image/box.png"));
		}

		else if (boxType == BOOSTJUMP){
			fixtureDef.restitution = 0.3f;
			boxSprite = new Sprite(new Texture("image/boostUp.gif"));
		}
		
		else if (boxType == SPEEDXMINUS){
			fixtureDef.restitution = 0.3f;
			boxSprite = new Sprite(new Texture("image/boostLeft.gif"));
		}
		
		else if (boxType == SPEEDXPLUS){
			fixtureDef.restitution = 0.3f;
			boxSprite = new Sprite(new Texture("image/boostRight.gif"));
		}


		box = world.createBody(bodyDef);
		boxFixture = box.createFixture(fixtureDef);

		boxSprite.setSize(halfWidth * 2, halfHeight * 2);		//Scaling the sprite to be the same size as the box, needed since sprite uses pixels, and Box2d uses meters
		boxSprite.setOrigin(boxSprite.getWidth() / 2, boxSprite.getHeight() / 2); //Setting the origin to the middle of the sprite
		box.setUserData(boxSprite);

		boxShape.dispose();

	}

	public void destroy(){
		world.destroyBody(box);
	}

	public Body bodyPosition(){
		return box;
	}

	public void setAngle(int angle){
		box.setTransform(box.getPosition().x,box.getPosition().y, MathUtils.degRad *  angle);
	}
	

	


}
