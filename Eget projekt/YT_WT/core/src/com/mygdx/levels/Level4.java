package com.mygdx.levels;

import java.util.LinkedList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.mygdx.entites.BackgroundMusic;
import com.mygdx.entites.Ball;
import com.mygdx.entites.Box;
import com.mygdx.game.InputController;
import com.mygdx.screens.Menu;

public class Level4 implements Screen {

	private World world;
	private SpriteBatch batch;											//Used to draw Sprites
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;									//Used to capture what is happening in the world, and to follow an object and zoom
	private final float TIMESTEP = 1 / 60f;  							//Common value
	private final int VELOCITYITERATION = 8, POSITIONITERATION = 3;		//Common value, the higher the values, the higher the quality
	private Body startPoint, endPoint;
	private Vector2 movement = new Vector2(), startCordinates, endCordinates;									//Used fof giving the force its direction in x and y.
	private Sprite boxSprite, ballSprite, backgroundSprite;											//The Sprite we want to use within the box
	private BodyDef bodyDef;													//A body definition
	private FixtureDef fixtureDef;
	private boolean ballCreated = false;
	private boolean cursor = false;
	private float zoom = 2.5f;
	private Vector2 boxDimensions;
	private int angle = 0, cameraAngle = 0;
	private Array<Body> tmpBodies = new Array<Body>(); 					//An array to store all the bodies in the world in, to be able to render them 
	private Ball ball;
	private Fixture endFixture;
	private Sound click, scroll;
	private Box currentBox;
	private LinkedList<Box> boxes = new LinkedList<Box>();
	
	private String message;
	private BitmapFont font;
	private int boxType = 1;
	private int tempX, tempY;
	private BackgroundMusic backgroundMusic;
	private boolean music = true;
	
	public Level4(BackgroundMusic backGroundMusic){
		this.backgroundMusic = backGroundMusic;
	}


	@Override
	public void render(float delta) {
		Gdx.input.setCursorCatched(!cursor);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		world.step(TIMESTEP, VELOCITYITERATION, POSITIONITERATION);					//Updates the world
		camera.zoom = zoom;
		
		if(ballCreated)
			message = ball.message();
			

		if(cameraAngle % 3 == 0){
			camera.position.set(0, 0, 0);
		}
		else if (cameraAngle % 3 == 1 && !ballCreated){
			camera.position.set(currentBox.bodyPosition().getPosition().x, currentBox.bodyPosition().getPosition().y, 0);
		}
		else if(cameraAngle % 3 == 2 && ballCreated){
			camera.position.set(ball.position().getPosition().x, ball.position().getPosition().y, 0);
		}
		else
			camera.position.set(0, 0, 0);

		camera.update();

		batch.setProjectionMatrix(camera.combined);									//Draw the sprites at the same position with the camera
		batch.begin();	
		//Begin to draw the batch
		backgroundSprite.draw(batch);;
		world.getBodies(tmpBodies); 												//Put the bodies that exists in the world in the array
		for (Body body: tmpBodies){													//For every body in the array
			if (body.getUserData() instanceof Sprite && body.getUserData() != null){													//If the userdata is an instance of Sprite	
				Sprite sprite = ((Sprite) body.getUserData());																			//Cast the userdata into a sprite
				sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);		//Set the sprites position to the same as the body, some extras to put the sprite down in the left corner.
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);														//Set the angle to the same as the body
				sprite.draw(batch);													//Draws the sprite on the batch						
			}
		}
		if(ballCreated)
		font.draw(batch, message, 0, 3);
		
		batch.end();																//Ends the batch

		//debugRenderer.render(world, camera.combined);								//Shows the outlines of the shapes.
	}

	@Override
	public void show() {
		world = new World(new Vector2(0, -9.81f), true);			//Gravity in x and y axis, are items allowed to sleep when not active
		debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		backgroundSprite = new Sprite(new Texture("image/background.jpg"));
		backgroundSprite.setSize(300, 175);
		backgroundSprite.setCenter(0, 0);
		font =  new BitmapFont(Gdx.files.internal("font/cartoon.fnt"));
		boxDimensions = new Vector2(7.5f, 1.5f);

		bodyDef = new BodyDef();				//A body definition
		fixtureDef = new FixtureDef();
		
		click = Gdx.audio.newSound(Gdx.files.internal("sounds/click.wav"));
		scroll = Gdx.audio.newSound(Gdx.files.internal("sounds/scroll.wav"));

		currentBox = new Box(world, new Vector2(0,0), boxDimensions.x, boxDimensions.y, angle, boxType);	

		//Body definition
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0, 0);

		//Fixture definition
		fixtureDef.friction = 0.5f;
		fixtureDef.restitution = 0f;
		startCordinates = new Vector2(0, -30);
		endCordinates = new Vector2(0, 20);

		//StartPoint

		bodyDef.position.set(startCordinates.x, startCordinates.y);  					//Starting position
		CircleShape start = new CircleShape();
		start.setRadius(1f);

		fixtureDef.shape = start;

		startPoint = world.createBody(bodyDef);
		startPoint.createFixture(fixtureDef);
		
		ballSprite = new Sprite(new Texture("image/Moon.png"));
		ballSprite.setSize(3.5f, 3.5f);		//Scaling the sprite to be the same size as the box, needed since sprite uses pixels, and Box2d uses meters
		ballSprite.setOrigin(ballSprite.getWidth() / 2, ballSprite.getHeight() / 2); //Setting the origin to the middle of the sprite
		startPoint.setUserData(ballSprite);

		start.dispose();

		//EndPoint
		bodyDef.position.set(endCordinates.x, endCordinates.y);  					//Starting position
		PolygonShape end = new PolygonShape();
		end.setAsBox(2f, 2f);

		fixtureDef.shape = end;

		endPoint = world.createBody(bodyDef);
		endFixture = endPoint.createFixture(fixtureDef);
		
		boxSprite = new Sprite(new Texture("image/finish.png"));
		boxSprite.setSize(2f * 2, 2f * 2);		//Scaling the sprite to be the same size as the box, needed since sprite uses pixels, and Box2d uses meters
		boxSprite.setOrigin(boxSprite.getWidth() / 2, boxSprite.getHeight() / 2); //Setting the origin to the middle of the sprite
		endPoint.setUserData(boxSprite);

		end.dispose();
		
	
		boxes.add(new Box(world, new Vector2(0,- 15), 25f, 2.5f, 0, Box.NORMAL));

		Gdx.input.setInputProcessor(new InputMultiplexer(new InputController(){ //A multiPlexer is used to be able to have more listeners to the keyboard and mouse. The chain is abled and disabled with false and true.
			@Override
			public boolean keyDown(int keycode) {
				switch(keycode){
				case Keys.ESCAPE:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Menu(backgroundMusic));
					break;
					
				case Keys.M:
					if(music){
						backgroundMusic.pause();
						music = false;
					}else{
						backgroundMusic.resume();
						music = true;
					}
					break;

				case Keys.N:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Level4(backgroundMusic));
					break;
					
				case Keys.NUM_1:
					
					if(!ballCreated){
					//Normal
					boxType = 1;
					currentBox.destroy();
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
					}
					break;
					
				case Keys.NUM_2:
					
					if(!ballCreated){
					//Bouncy
					boxType = 2;
					currentBox.destroy();
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
					break;
					}
					
				case Keys.NUM_3:
					if(!ballCreated){
					//Impulse
					boxType = 3;
					currentBox.destroy();
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
					}
					break;
					
				case Keys.NUM_4:
					if(!ballCreated){
					//Impulse
					boxType = 4;
					currentBox.destroy();
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
					}
					break;
					
				case Keys.NUM_5:
					if(!ballCreated){
					//Impulse
					boxType = 5;
					currentBox.destroy();
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
					}
					break;
					

				case Keys.R:
					//If the ball is created, destroy the ball and create a new box of placement, followed by a static starting point, just showing where the ball will spawn.
					if(ballCreated){
						ball.destroy();
						ballCreated = false;

						currentBox = new Box(world, new Vector2(0,0), boxDimensions.x, boxDimensions.y, angle, boxType);

						//Shows the ball spawn point again
						bodyDef.position.set(startCordinates.x, startCordinates.y);  					
						CircleShape start = new CircleShape();
						start.setRadius(1f);

						fixtureDef.shape = start;

						startPoint = world.createBody(bodyDef);
						startPoint.createFixture(fixtureDef);
						startPoint.setUserData(ballSprite);

						start.dispose();

					}
					break;

				case Keys.B:
					//if the ball is already created, this just moves the ball to the selected spawn point, rotates it to default and sets it to sleep to remove previous movement
					if(ballCreated){
						ball.restart();
					}

					//This creates a dynamic ball if not a ball already has been created
					if (!ballCreated){
						world.destroyBody(startPoint);
						currentBox.destroy();
						
						ball = new Ball(world, startCordinates, endFixture, boxes);	
						ballCreated = true;
					}
					break;

				case Keys.C:
					cameraAngle++;
					break;

				case Keys.PLUS:
					zoom -= 3 / 10f;
					break;

				case Keys.MINUS:
					zoom += 3/ 10f;
					break;
					
				case Keys.RIGHT:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Level5(backgroundMusic));
					break;
					
				case Keys.LEFT:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Level3(backgroundMusic));

				}
				return false;  //In this case, the multiplexer sends further to the car, because this was false. (Not false in the meaning that this commands not should be executed, byt in the meaning that this was not the only commands to be checked by the mutliplexer.
			}

			@Override
			public boolean keyUp(int keycode) {
				movement.set(0, 0);  //This dont care about which key is pressed, you cant make the same as above and set to 0 instead for each key.
				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				if(!ballCreated){
					scroll.play();
					angle += amount * 5;
					currentBox.setAngle(angle);
				}
				return true;

			}

			public boolean mouseMoved(int screenX, int screenY) {
				tempX = screenX - Gdx.graphics.getWidth() / 2;
				tempY = screenY - Gdx.graphics.getHeight() / 2;
				if(!ballCreated){
					currentBox.bodyPosition().setTransform(tempX / 13f, - tempY / 13f, MathUtils.degRad *  angle);
				}
				return true;
			}
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				tempX = screenX - Gdx.graphics.getWidth() / 2;
				tempY = screenY - Gdx.graphics.getHeight() / 2;
				
				if(!ballCreated){
					click.play();
					boxes.add(currentBox);
					currentBox = new Box(world, new Vector2(tempX / 13f, - tempY /13f), boxDimensions.x, boxDimensions.y, angle, boxType);
				}

				return true;
			};
		} )); //efter } kan ett , och en annan inputcontroller f�lja som ska lyssna, var tidigare car.

	}

	@Override
	public void resize(int width, int height) {
		camera.viewportHeight = height / 25;			//Zooms in
		camera.viewportWidth = width / 25;
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		world.dispose();
		debugRenderer.dispose();
		//boxSprite.getTexture().dispose();
		click.dispose();
		scroll.dispose();
	}

}
