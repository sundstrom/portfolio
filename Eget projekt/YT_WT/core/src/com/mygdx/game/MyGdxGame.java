package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.screens.Splash;

public class MyGdxGame extends Game{

	public static final String TITLE = "Keep On Rollin'", VERSION = "1.0";

	@Override
	public void create () {
		setScreen(new Splash());
	}

	@Override
	public void render () {
		super.render();
	}

}
