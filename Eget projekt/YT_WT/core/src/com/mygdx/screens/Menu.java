package com.mygdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.entites.BackgroundMusic;
import com.mygdx.game.InputController;
import com.mygdx.levels.Level1;

public class Menu implements Screen {

	private SpriteBatch batch;
	private BitmapFont font;
	private BitmapFont.TextBounds bounds;
	private String instruction = "Play - P\n" + "How to play: H\n" + "Settings - S\n" + "Exit - Esc";
	private BackgroundMusic music;
	private boolean play = true;
	private boolean cursor = true;
	
	public Menu(BackgroundMusic music){
		this.music = music;
	}

	@Override
	public void show() {	
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("font/cartoon.fnt"));

		Gdx.input.setInputProcessor(new InputController(){

			@Override
			public boolean keyDown(int keycode) {
				switch(keycode){
				case Keys.P:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Level1(music));
					break;

				case Keys.S:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Settings(music));
					break;

				case Keys.H:
					((Game) Gdx.app.getApplicationListener()).setScreen(new HowTo(music));
					break;
					
				case Keys.M:
					if(play){
						music.pause();
						play = false;
					}else{
						music.resume();
						play = true;
					}
					break;
				
				case Keys.Q:
					if(!cursor){
						cursor = true;
					}else{
						cursor = false;
					}
					break;
					

				case Keys.ESCAPE:
					Gdx.app.exit();
					break;
				}
				return true;
			}
		});
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.input.setCursorCatched(cursor);

		batch.begin();
		bounds = font.getBounds("MENU");
		font.draw(batch, "MENU", Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2 + 250 );

		bounds = font.getMultiLineBounds(instruction);
		font.drawMultiLine(batch, instruction, Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2);

		batch.end();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

}
