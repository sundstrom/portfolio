package com.mygdx.screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.entites.BackgroundMusic;
import com.mygdx.tween.SpriteAccessor;

public class Splash implements Screen{
	private SpriteBatch batch;
	private Sprite splash;
	private TweenManager tweenManager;
	BackgroundMusic music = new BackgroundMusic();
	
	@Override
	public void show() {
		Gdx.input.setCursorCatched(true);
		
		
		Thread musicThread = new Thread(music);
		musicThread.start();
		
		batch = new SpriteBatch();
		tweenManager = new TweenManager();
		Tween.registerAccessor(Sprite.class, new SpriteAccessor());
		
		
		Texture splashTexture = new Texture("image/Splash2.jpg");
		splash = new Sprite(splashTexture);
		splash.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		Tween.set(splash, SpriteAccessor.ALPHA).target(0).start(tweenManager); //S�tter startv�rdet till 0, helt transparent
		Tween.to(splash, SpriteAccessor.ALPHA, 2f).target(1).repeatYoyo(1, 1).setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new Menu(music));
				
			}
		}).start(tweenManager); //Alpha v�rdet ska �ndras till 1 �ver 4 sekunder. Sen ska det g� tillbaka med en repetition �ver 2 sekunder.
		
	}


	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);			//The color we want to clear with
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	//Clearing with the color
		
		tweenManager.update(delta);
		
		batch.begin();
		
		splash.draw(batch);
		
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		splash.getTexture().dispose();
	}

}
