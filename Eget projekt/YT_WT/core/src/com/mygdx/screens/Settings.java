package com.mygdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.entites.BackgroundMusic;
import com.mygdx.game.InputController;

public class Settings implements Screen {

	private SpriteBatch batch;
	private BitmapFont font;
	private BitmapFont.TextBounds bounds;
	private BackgroundMusic music;
	private boolean playMusic = true;
	private String instruction = "Menu: Esc\n" + "Place box: Left mouse\n" + "Changing angle of the box: Scrollwheel\n" 
	+ "Boxes:\n" + "Normal - 1 Trampoline - 2\n" + "Jumpboost - 3 Rightboost = 4 Leftboost = 5\n" + "Zoom: + - \n" + "Cameraview: C (centered, box, ball)\n" + "Insert ball: B\n" 
	+ "To remove the ball: R\n" + "Music: M\n" + "Restart: N\n" + "Change level: Left & Right Key";

	public Settings(BackgroundMusic music){
		this.music = music;
	}
	
	@Override
	public void show() {
		Gdx.input.setCursorCatched(true);
		
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("font/settings.fnt"));

		Gdx.input.setInputProcessor(new InputController(){

			@Override
			public boolean keyDown(int keycode) {
				switch(keycode){
				case Keys.ESCAPE:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Menu(music));
					break;	
				
			case Keys.M:
				if(playMusic){
					music.pause();
					playMusic = false;
				}else{
					music.resume();
					playMusic = true;
				}
				break;
				
		}
				return true;
			}
			});

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		bounds = font.getBounds("SETTINGS");
		font.draw(batch, "SETTINGS", Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2 + 250 );

		bounds = font.getMultiLineBounds(instruction);
		font.drawMultiLine(batch, instruction, Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2);

		batch.end();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

}
