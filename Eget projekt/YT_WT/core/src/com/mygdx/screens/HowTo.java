package com.mygdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.entites.BackgroundMusic;
import com.mygdx.game.InputController;

public class HowTo implements Screen {

	private SpriteBatch batch;
	private BitmapFont font;
	private BitmapFont.TextBounds bounds;
	private String instruction = "Get the ball to the finish line.\n"+ "To your help you have different kind of boxes to place out.";
	private BackgroundMusic backGroundMusic;
	private boolean music = true;
	
	public HowTo(BackgroundMusic backGroundMusic){
		this.backGroundMusic = backGroundMusic;
	}

	@Override
	public void show() {
		Gdx.input.setCursorCatched(true);
		
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("font/cartoon.fnt"));
		font.setScale(.90f);
		

		Gdx.input.setInputProcessor(new InputController(){

			@Override
			public boolean keyDown(int keycode) {
				switch(keycode){
				case Keys.ESCAPE:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Menu(backGroundMusic));
					break;
					
				case Keys.M:
					if(music){
						backGroundMusic.pause();
						music = false;
					}else{
						backGroundMusic.resume();
						music = true;
					}
					break;
				}
				return true;
		}
			});

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		bounds = font.getBounds("HOW TO PLAY");
		font.draw(batch, "HOW TO PLAY", Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2 + 250 );
		bounds = font.getMultiLineBounds(instruction);
		font.drawMultiLine(batch, instruction, Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() / 2 + bounds.height / + 2);

		batch.end();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

}
