package com.mygdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.InputController;

public class Walkthrough implements Screen {

	private World world;
	private SpriteBatch batch;											//Used to draw Sprites
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;									//Used to capture what is happening in the world, and to follow an object and zoom
	private final float TIMESTEP = 1 / 60f;  							//Common value
	private final int VELOCITYITERATION = 8, POSITIONITERATION = 3;		//Common value, the higher the values, the higher the quality

	private Body ball;
	private int speed = 500;											//The speed that is the limit for the force when we use WASD
	Vector2 movement = new Vector2();									//Used fof giving the force its direction in x and y.
	private Sprite boxSprite;											//The Sprite we want to use within the box
	
	private Array<Body> tmpBodies = new Array<Body>(); 					//An array to store all the bodies in the world in, to be able to render them 
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		world.step(TIMESTEP, VELOCITYITERATION, POSITIONITERATION);					//Updates the world
		ball.applyForceToCenter(movement, true);										//Applies force to the box, movement is set to x and y 0, 0 as default. Altered when pressing WASD.
		camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(ball.getPosition().x, ball.getPosition().y, 0); 			//Uppdate the cameras position to be on the box, o is for z value which is not used since it is an 2d world.
		camera.update(); 															//Updates the camera after the position has been changed AND everytime that we render
		
		batch.setProjectionMatrix(camera.combined);									//Draw the sprites at the same position with the camera
		batch.begin();																//Begin to draw the batch
		world.getBodies(tmpBodies); 												//Put the bodies that exists in the world in the array
		for (Body body: tmpBodies){													//For every body in the array
			if (body.getUserData() instanceof Sprite && body.getUserData() != null){													//If the userdata is an instance of Sprite	
				Sprite sprite = ((Sprite) body.getUserData());																			//Cast the userdata into a sprite
				sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);		//Set the sprites position to the same as the body, some extras to put the sprite down in the left corner.
				sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);														//Set the angle to the same as the body
				sprite.draw(batch);													//Draws the sprite on the batch						
			}
		}
		batch.end();																//Ends the batch
		
		debugRenderer.render(world, camera.combined);								//This is what renders the world.... I think...
	}

	@Override
	public void show() {
		if (Gdx.app.getType() == ApplicationType.Desktop){
			Gdx.graphics.setDisplayMode((int) (Gdx.graphics.getHeight() / 1.5f), Gdx.graphics.getHeight(), false);
		}
		
		world = new World(new Vector2(0, -9.81f), true);			//Gravity in x and y axis, are items allowed to sleep when not active
		debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();

		

		BodyDef bodyDef = new BodyDef();				//A body definition
		FixtureDef fixtureDef = new FixtureDef();

		//BOX
		//Body definition
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(2.25f, 10);

		//Box shape
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(.5f, 1f);

		//Fixture definition
		fixtureDef.shape = boxShape;
		fixtureDef.density = 5;
		fixtureDef.friction = .75f;
		fixtureDef.restitution = .1f;
//
		ball = world.createBody(bodyDef);
		ball.createFixture(fixtureDef);
//
		boxSprite = new Sprite(new Texture("image/luigifront.png"));
		boxSprite.setSize(0.5f * 2, 1f * 2);		//Scaling the sprite to be the same size as the box, needed since sprite uses pixels, and Box2d uses meters
		boxSprite.setOrigin(boxSprite.getWidth() / 2, boxSprite.getHeight() / 2); //Setting the origin to the middle of the sprite
		ball.setUserData(boxSprite);
//
		boxShape.dispose();

		//BALL

		//Body Definition
		bodyDef.type = BodyType.DynamicBody;			//Type of body			//Following two lines are commented out due to that this shape has been attched to the box, and are the same body.
		bodyDef.position.set(0, 10);  					//Starting position


		//Ball shape
		CircleShape ballShape = new CircleShape();
		//ballShape.setPosition(new Vector2(0, 1.50f));  //To move the circle out of the boxbody
		ballShape.setRadius(.5f);


		//Fixture definition, the physical properties
		fixtureDef.shape = ballShape;
		fixtureDef.density = 5f;  //In kg per m2.
		fixtureDef.friction = 1f;
		fixtureDef.restitution = 0.3f;

		ball = world.createBody(bodyDef);
		ball.createFixture(fixtureDef);

		ballShape.dispose(); //Se dess f�rklaring

		//GROUND

		//Body definition
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0, 0);

		//Shape definition
		ChainShape groundShape = new ChainShape();
		groundShape.createChain(new Vector2[]{new Vector2(-50, 50), new Vector2(-50,0), new Vector2(50,0), new Vector2(50, 50)});  //Creating two points for the ground,  wich will be connected with a line.


		//Fixture definition
		fixtureDef.shape = groundShape;
		fixtureDef.friction = 0.5f;
		fixtureDef.restitution = 0f;

		Body ground = world.createBody(bodyDef);
		ground.createFixture(fixtureDef);

		groundShape.dispose();
		
//		//OTHER BOX
		bodyDef.position.y = 7;
//		
		PolygonShape otherBoxShape = new PolygonShape();
		otherBoxShape.setAsBox(.25f, .25f);
//		
		fixtureDef.shape = otherBoxShape;
//		
		Body otherBox = world.createBody(bodyDef);
		otherBox.createFixture(fixtureDef);
//		
		otherBoxShape.dispose();
		
		//DistanceJoint between other box and box
		DistanceJointDef distanceJointDef = new DistanceJointDef();
		distanceJointDef.bodyA = otherBox;  //It doesn't matter which is A or B
		distanceJointDef.bodyB = ball;
		distanceJointDef.length = 5;
		
		world.createJoint(distanceJointDef);
		
		//RopeJoint between ground and Box
		RopeJointDef ropeJointDef = new RopeJointDef();
		ropeJointDef.bodyA = ball;
		ropeJointDef.bodyB = ground;
		ropeJointDef.maxLength = 7;
		ropeJointDef.localAnchorA.set(0, 0);
		ropeJointDef.localAnchorB.set(0, 0);
		
		
		world.createJoint(ropeJointDef);
		
		//CAR
		
		FixtureDef wheelFixtureDef = new FixtureDef();
		
		fixtureDef.density = 5;
		fixtureDef.friction = .4f;
		fixtureDef.restitution = .3f;
		
		wheelFixtureDef.density = fixtureDef.density* 1.5f;
		wheelFixtureDef.friction = 50;
		wheelFixtureDef.restitution = .4f;
		
		Gdx.input.setInputProcessor(new InputMultiplexer(new InputController(){ //A multiPlexer is used to be able to have more listeners to the keyboard and mouse. The chain is abled and disabled with false and true.
			@Override
			public boolean keyDown(int keycode) {
				switch(keycode){
				case Keys.ESCAPE:
					((Game) Gdx.app.getApplicationListener()).setScreen(new Walkthrough());

					break;
				case Keys.W:
					movement.y = speed;
					break;				
				case Keys.A:			
					movement.x = - speed;
					break;
				case Keys.S:
					movement.y = - speed;
					break;
				case Keys.D:
					movement.x = speed;
					break;

				}
				return false;  //In this case, the multiplexer sends further to the car, because this was false. (Not false in the meaning that this commands not should be executed, byt in the meaning that this was not the only commands to be checked by the mutliplexer.
			}

			@Override
			public boolean keyUp(int keycode) {
				movement.set(0, 0);  //This dont care about which key is pressed, you cant make the same as above and set to 0 instead for each key.
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				camera.zoom += amount / 10f;
				return true;
			}
		} )); //efter } kan ett , och en annan inputcontroller f�lja som ska lyssna, var tidigare car.


	}

	

	@Override
	public void resize(int width, int height) {
		camera.viewportHeight = height / 25;			//Zooms in
		camera.viewportWidth = width / 25;

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		world.dispose();
		debugRenderer.dispose();
		boxSprite.getTexture().dispose();
	}

}
