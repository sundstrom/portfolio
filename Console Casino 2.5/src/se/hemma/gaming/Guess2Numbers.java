package se.hemma.gaming;

import java.util.Random;

public class Guess2Numbers {

	private Random dice = new Random();
	private Bank bank;
	private Statistics statistics;
	private Bet bet;
	private Highscore highscore;


	public void setAll(Bet bet, Statistics statistics, Bank bank, Highscore highscore){
		this.bet = bet;
		this.statistics = statistics;
		this.bank = bank;
		this.highscore = highscore;
	}

	public void play() {

		int myAnswer;
		int rightAnswer = 1 + dice.nextInt(2);
		System.out.print("The answer is either 1 or 2, make a choice: ");

		while (true)
		{
			Double getDouble = Number.TryParseDouble();
			myAnswer =  getDouble.intValue();
			if(myAnswer >= 3 || myAnswer <= 0 )
			{
				System.out.println();
				System.err.print("You didn't make a right choice, try again: ");
			}
			else
				break;
		}

		System.out.println();

		if(myAnswer == rightAnswer){
			System.out.print("Congratulations! You have won " + GetFormatedNumbers.getFormatedNumber(bet.getYourBet() * 2) + "!");
			bank.update(Bank.WIN, bet.getYourBet() * 2);
			statistics.update(Statistics.WIN, bet.getYourBet() * 2);
			highscore.update();
		}


		if (myAnswer != rightAnswer){
			System.out.println("Too bad, that was not the right number, you lost your bet!");
			System.out.println("The right number was " + rightAnswer +".");
			bank.update(Bank.LOOSE, bet.getYourBet());
			statistics.update(Statistics.LOOSE, bet.getYourBet());

		}
	}
}
