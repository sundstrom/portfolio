package se.hemma.gaming;

public class GameMenu 
{
	private Guess2Numbers game1;
	private Guess3Numbers game2;
	private Lotto game3;
	
	
	public void setAll(Guess2Numbers game1, Guess3Numbers game2, Lotto game3 ){
		this.game1 = game1;
		this.game2 = game2;
		this.game3 = game3;
	}
	
	public void chooseGame()
	{
		System.out.println();
		System.out.println("Choose your game:");
		System.out.println("1. Guess the right number out of 2 choices, and double your bet");
		System.out.println("2. Guess the right number out of 3 choices, and triple your bet");
		System.out.println("3. Play a game of lotto and win 5 times your bet");
		
		boolean rightChoice = false;

		do
		{
			Double getDouble = Number.TryParseDouble();
			int choice = getDouble.intValue();
			
			switch(choice)
			{
			case 1:
				System.out.println();
				game1.play();
				rightChoice = true;
				break;

			case 2:
				System.out.println();
				game2.play();
				rightChoice = true;
				break;

			case 3:
				System.out.println();
				game3.play();
				rightChoice = true;
				break;

			default:
				System.err.print("You didn't make a right choice, try again: ");
			}
		}
		while(rightChoice == false);
	}
}
