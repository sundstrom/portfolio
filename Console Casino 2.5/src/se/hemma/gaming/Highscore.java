package se.hemma.gaming;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Highscore {
	private ArrayList<Double> list = new ArrayList<Double>();
	private double currentHighscore;	//Highscore f�r denna omg�ng
	private static double highscore;	//Denna �r highscoren f�r alla omg�ngar
	private Bank myBank;
	private ObjectOutputStream oos = null;
	private ObjectInputStream ois = null;
	public static final boolean RESETALLINCLUDINGSTATICS = true;
	public static final boolean RESETONLYINSTANCEVARIABLES = false;

	@SuppressWarnings("unchecked")
	public Highscore(){
		try{
			ois = new ObjectInputStream(new FileInputStream("Highscore.dat"));
			list = (ArrayList<Double>) ois.readObject();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (ois != null)
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		currentHighscore = list.get(0);
		highscore = list.get(1);

		
	}

	public void setBank(Bank myBank){
		this.myBank = myBank;
	}

	public double getCurrentHighscore() {
		return currentHighscore;
	}

	public void setCurrentHighscore(double currentHighscore) {
		this.currentHighscore = currentHighscore;
	}

	public static double getHighscore() {
		return highscore;
	}

	public static void setHighscore(double highscore) {
		Highscore.highscore = highscore;
	}

	public void update(){

		if(myBank.getYourBank() > highscore && myBank.getYourBank() > currentHighscore)
		{
			highscore = myBank.getYourBank();
			currentHighscore = highscore;
			System.out.print(" You have set a new alltime highscore of " + GetFormatedNumbers.getFormatedNumber(highscore) +" in your bank!");

		}
		else if(myBank.getYourBank() == highscore)
		{
			currentHighscore = myBank.getYourBank();
			System.out.print(" You have also touched the alltime highscore of " + GetFormatedNumbers.getFormatedNumber(currentHighscore) +" in your bank!");

		}

		else if(myBank.getYourBank() > currentHighscore)
		{
			currentHighscore = myBank.getYourBank();
			System.out.print(" You have also set a new round highscore of " + GetFormatedNumbers.getFormatedNumber(currentHighscore) +" in your bank!");

		}
		list = new ArrayList<Double>();
		list.add(currentHighscore);
		list.add(highscore);
		
		try{
			oos = new ObjectOutputStream(new FileOutputStream("Highscore.dat"));
			oos.writeObject(list);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if (oos != null) oos.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		System.out.println();	
	}

	public void reset(boolean totalReset){
		currentHighscore = 1000;
		if (totalReset){
			highscore = 1000;
		}
		
		list = new ArrayList<Double>();
		list.add(currentHighscore);
		list.add(highscore);
		
		try{
			oos = new ObjectOutputStream(new FileOutputStream("Highscore.dat"));
			oos.writeObject(list);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if (oos != null) oos.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
}


