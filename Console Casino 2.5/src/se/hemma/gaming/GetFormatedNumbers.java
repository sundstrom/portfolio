package se.hemma.gaming;

import java.text.NumberFormat;

public class GetFormatedNumbers {

	private static NumberFormat nf = NumberFormat.getNumberInstance();


	public static String getFormatedNumber(double number)
	{
		nf.setMaximumFractionDigits(2);			//Formaterar hur siffrorna visas
		nf.setMinimumFractionDigits(2);

		return nf.format(number);
	}


}
