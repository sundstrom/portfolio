package se.hemma.gaming;

import java.util.Scanner;

public class Start 
{

	public static void main(String[] args) 
	{
		Bank bank = new Bank();
		Bet bet = new Bet();
		GameMenu gameMenu = new GameMenu();	
		Highscore highscore = new Highscore();
		Statistics statistics = new Statistics();
		Guess2Numbers game1 = new Guess2Numbers();
		Guess3Numbers game2 = new Guess3Numbers();
		Lotto game3 = new Lotto();
		
		bank.setBet(bet);
		bet.setAll(bank, statistics);
		gameMenu.setAll(game1, game2, game3);
		highscore.setBank(bank);
		statistics.setAll(bet, highscore);
		game1.setAll(bet, statistics, bank, highscore);
		game2.setAll(bet, statistics, bank, highscore);
		game3.setAll(bet, statistics, bank, highscore);

		System.out.println("Welcome to Console Casino 2.5!");
		Scanner input = new Scanner(System.in);
		boolean playAgain = true;

		do
		{
			while (bank.getYourBank() > 0){
				System.out.println();
				System.out.println("You have " + bank.getFormatedyourBank() + " in your bank.");
				bet.setYourBet();
				gameMenu.chooseGame(); //Startar spelet
						
			}

			System.out.println();
			System.out.println("You're out of money, game over!");
			System.out.println();
			System.out.print("Do you want to play another round or do you wish to reset all statistics? (Y | N | R) : ");		

			while(true)
			{
				String answer = input.nextLine();	
				if (answer.equalsIgnoreCase("y"))
				{
					statistics.setRounds();			//F�r varje ny runda jag startar �kar denna en r�knare i Game med 1.
					bank.reset();
					highscore.reset(Highscore.RESETONLYINSTANCEVARIABLES);
					statistics.reset(Statistics.RESETONLYINSTANCEVARIABLES);
					break;
				}

				else if (answer.equalsIgnoreCase("n"))
				{
					statistics.setRounds();			//F�r varje ny runda jag startar �kar denna en r�knare i Game med 1.
					bank.reset();
					highscore.reset(Highscore.RESETONLYINSTANCEVARIABLES);
					statistics.reset(Statistics.RESETONLYINSTANCEVARIABLES);
					playAgain = false;
					break;
				}
				
				else if (answer.equalsIgnoreCase("r")){
					bank.reset();
					highscore.reset(Highscore.RESETALLINCLUDINGSTATICS);
					statistics.reset(Highscore.RESETALLINCLUDINGSTATICS);
					System.out.println();
					System.out.println("Console Casino has been reset");
					System.out.print("Do you want to play another round?(Y | N ) : ");
				}

				else
				{
					System.err.print("Enter a valid choice, (Y | N) ");
				}
			}
		}
		while(playAgain);

		System.out.println();
		System.out.println("Thanks for playing, have a nice day!");
		input.close();

	}
}
