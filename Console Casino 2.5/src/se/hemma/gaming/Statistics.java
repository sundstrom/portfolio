package se.hemma.gaming;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Statistics  {

	private ArrayList<Integer> listInt;
	private ArrayList<Double> listDouble;
	private int rounds;							//R�knar hur m�nga rundor jag spelat under en omg�ng
	private int gamesWon;						//R�knar hur m�nga g�nger jag vunnit under en omg�ng
	private int gamesLost;						//R�knar hur m�nga g�nger jag f�rlorat under en omg�ng
	private double amountWon;					//R�knar hur mycket jag vunnit under en omg�ng
	private double amountLost;					//R�knar hur mycket jag f�rlorat under en omg�ng
	private double amountBeted;					//R�knar hur mycket jag satsat under en omg�ng
	private static int games;					//R�knar hur m�nga spel jag k�rt
	private Bet bet;
	private Highscore highscore;
	private ObjectOutputStream oosInt = null;
	private ObjectOutputStream oosDouble = null;
	private ObjectInputStream oisInt = null;
	private ObjectInputStream oisDouble = null;
	public static final boolean RESETALLINCLUDINGSTATICS = true;
	public static final boolean RESETONLYINSTANCEVARIABLES = false;
	public static final boolean WIN = true;
	public static final boolean LOOSE = false;


	@SuppressWarnings("unchecked")
	public Statistics(){
		try{
			oisInt = new ObjectInputStream(new FileInputStream("StatisticsInt.dat"));
			oisDouble = new ObjectInputStream(new FileInputStream("StatisticsDouble.dat"));

			listInt = (ArrayList<Integer>) oisInt.readObject();
			listDouble = (ArrayList<Double>) oisDouble.readObject();

			rounds = listInt.get(0);
			gamesWon = listInt.get(1);
			gamesLost = listInt.get(2);
			games = listInt.get(3);

			amountWon = listDouble.get(0);
			amountLost = listDouble.get(1);
			amountBeted = listDouble.get(2);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if (oisInt != null) oisInt.close();
				if (oisDouble != null) oisDouble.close();
			} catch (Exception e){
				e.printStackTrace();
			}

		}
	}

	public void setAll(Bet bet, Highscore highscore ){
		this.bet = bet;
		this.highscore = highscore;
	}

	public void setBet(Bet bet){
		this.bet = bet;
	}

	public Highscore getMyScore() {
		return highscore;
	}



	public void setHighScore(Highscore myScore){
		this.highscore = myScore;
	}

	public void setRounds()			//Denna visar hur m�nga rundor jag har k�rt, en runda slutar n�r jag inte har n�gra pengar kvar. Denna metod anropas i Start.
	{
		games ++;
	}

	public void update(boolean win, double amount){
		if(win){
			amountWon = amountWon + amount;
			gamesWon ++;
			rounds ++;
			amountBeted = amountBeted + bet.getYourBet();
		}

		else{
			amountLost = amountLost + bet.getYourBet();		
			gamesLost ++;
			rounds ++;
			amountBeted = amountBeted + bet.getYourBet();
		}

		try {
			listInt = new ArrayList<Integer>();
			listDouble = new ArrayList<Double>();
			listInt.add(rounds);
			listInt.add(gamesWon);
			listInt.add(gamesLost);
			listInt.add(games);

			listDouble.add(amountWon);
			listDouble.add(amountLost);
			listDouble.add(amountBeted);

			oosInt = new ObjectOutputStream(new FileOutputStream("StatisticsInt.dat"));
			oosDouble = new ObjectOutputStream(new FileOutputStream("StatisticsDouble.dat"));

			oosInt.writeObject(listInt);
			oosDouble.writeObject(listDouble);

		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if (oosInt != null) oosInt.close();
				if (oosDouble != null) oosDouble.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void getStatistics()
	{
		if (rounds == 0 &&  games == 1){
			System.out.println();
			System.out.println("No statistics available since this is your first game and first run.");
			System.out.println();
		}
		else{
			System.out.println();
			System.out.println("--------- STATISTICS ----------");
			
			System.out.println();
			System.out.println("----------- OVERALL -----------");
			System.out.println("Total games played: " + games);
			System.out.println("Highest bank ever: " + GetFormatedNumbers.getFormatedNumber(Highscore.getHighscore()));
			
			System.out.println();
			System.out.println("--------- THIS ROUND ----------");
			System.out.println("Your highest bank this round is " + GetFormatedNumbers.getFormatedNumber(highscore.getCurrentHighscore()));
			System.out.println("Total rounds played this game is " + rounds);	
			System.out.println("You have won " + gamesWon + " games.");
			System.out.println("You have lost " + gamesLost + " games.");
			System.out.println();

			System.out.println("Total amount of your bets are " + GetFormatedNumbers.getFormatedNumber(amountBeted));
			System.out.println("Your average bet per game is " + GetFormatedNumbers.getFormatedNumber(amountBeted / rounds));
			System.out.println("You have won " + GetFormatedNumbers.getFormatedNumber(amountWon) + " on your bets.");
			System.out.println("You have lost " + GetFormatedNumbers.getFormatedNumber(amountLost) + " on your bets.");
			System.out.println();

			System.out.println("Your average win per game is " + GetFormatedNumbers.getFormatedNumber(amountWon / rounds));
			System.out.println("Your average loss per game is "+ GetFormatedNumbers.getFormatedNumber(amountLost / rounds));
			System.out.println("-------------------------------");
			System.out.println();
		}
	}

	public void reset(boolean totalReset){
		rounds = 0;
		gamesWon = 0;
		gamesLost = 0;
		amountWon = 0;
		amountLost = 0;
		amountBeted = 0;
		
		if (totalReset){
			games = 0;
		}
		
		try {
			listInt = new ArrayList<Integer>();
			listDouble = new ArrayList<Double>();
			listInt.add(rounds);
			listInt.add(gamesWon);
			listInt.add(gamesLost);
			listInt.add(games);

			listDouble.add(amountWon);
			listDouble.add(amountLost);
			listDouble.add(amountBeted);

			oosInt = new ObjectOutputStream(new FileOutputStream("StatisticsInt.dat"));
			oosDouble = new ObjectOutputStream(new FileOutputStream("StatisticsDouble.dat"));

			oosInt.writeObject(listInt);
			oosDouble.writeObject(listDouble);

		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if (oosInt != null) oosInt.close();
				if (oosDouble != null) oosDouble.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}




