package se.hemma.gaming;

import java.util.Scanner;

public class Number {
	
private static Scanner text = new Scanner(System.in);
	
	public static Double TryParseDouble() 
	{
		while(true)
		{

			String someText = text.next(); 
			try 
			{
				return Double.parseDouble(someText);			//Fungerar bara med. Kan man f� den att fungera med "," s� man kan anv�nda numpaden?
			} 

			catch (NumberFormatException ex) 
			{
				System.out.println();
				System.err.print("That is not a number, try again: ");
			}
		}
	}

}
