package se.hemma.gaming;

import java.util.Scanner;

public class Bet {

	private double yourBet;	
	private Bank bank;
	private Statistics statistics;
	private Scanner text = new Scanner(System.in);
	private boolean rightChoice = false;

	public void setAll(Bank bank, Statistics statistics){
		this.bank = bank;
		this.statistics = statistics;
	}

	public double getYourBet() {
		return yourBet;
	}

	public void setYourBet(){

		while (bank.getYourBank()> 0 && !rightChoice){					//S� l�nge som det jag har p� banken �r st�rre �n noll (s� l�nge jag kan satsa n�got)

			System.out.print("Enter the amount that you want to bet or enter 0 to see some statistics: ");
			
			double bet = Number.TryParseDouble();		//H�r skapas en dubbel som �r vad jag ska satsa, den g�r igenom en metod som kollar att det �r en double som l�ggs in.

			if (bet > bank.getYourBank()){					//Om det jag satsar �r mer �n vad jag har p� banken, felmeddelande	

				System.out.print("You do not have that much money. Do you wish to go all in with what you have (" + bank.getYourBank()+")? (Y or N) ");
				while(true)
				{
					String answer = text.next();	
					if (answer.equalsIgnoreCase("y"))
					{
						yourBet = bank.getYourBank();		
						rightChoice = true;
						break;
					}

					else if (answer.equalsIgnoreCase("n"))
					{
						System.out.println();
						break;
					}

					else
					{
						System.out.print("Enter a valid choice, Y or N: ");
					}
				}
			}

			else if (bet == 0)					//Funktion f�r att ta in statistik
			{
				statistics.getStatistics();	
			}

			else if (bet < 0)
			{
				System.out.println();
				System.err.println("Invalid bet, place a bet up to " + GetFormatedNumbers.getFormatedNumber(bank.getYourBank()));
			}

			else
			{
				yourBet = bet;
				rightChoice = true;
				break;
			}
		}
		rightChoice = false;
	}
}
