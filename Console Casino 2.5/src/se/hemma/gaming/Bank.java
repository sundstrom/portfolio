package se.hemma.gaming;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Bank  {

	private double saldo;	
	private Bet bet;
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;
	public static final boolean WIN = true;
	public static final boolean LOOSE = false;

	public Bank (){
		try {
			ois = new ObjectInputStream(new FileInputStream("saldo.dat"));
			saldo = (double) ois.readObject();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (ois != null)
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	public void setBet(Bet bet){
		this.bet = bet;
	}

	public double getYourBank() {
		return saldo;
	}

	public String getFormatedyourBank(){
		return GetFormatedNumbers.getFormatedNumber(saldo);
	}

	public void update(boolean win, double number){

		if(win){
			this.saldo = saldo + number;						
		}

		else{
			this.saldo = saldo - bet.getYourBet();		
		}
		
		try {
			oos = new ObjectOutputStream(new FileOutputStream("saldo.dat"));
			oos.writeObject(saldo);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if (oos != null)
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	public void reset(){
		saldo = 1000;
		
		try {
			oos = new ObjectOutputStream(new FileOutputStream("saldo.dat"));
			oos.writeObject(saldo);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if (oos != null)
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
