package se.hemma.gaming;

import java.util.Random;

public class Guess3Numbers {
	private Random dice = new Random();
	private Bank bank;
	private Bet bet;
	private Statistics statistics;
	private Highscore highscore;


	public void setAll(Bet bet, Statistics statistics, Bank bank, Highscore highscore){
		this.bet = bet;
		this.statistics = statistics;
		this.bank = bank;
		this.highscore = highscore;
	}

	public void play()
	{

		int myAnswer;
		int rightAnswer = 1 + dice.nextInt(3);
		System.out.print("The answer is either 1, 2 or 3, make a choice: ");

		while (true)
		{
			Double getDouble = Number.TryParseDouble();
			myAnswer =  getDouble.intValue();

			if(myAnswer >= 4 || myAnswer <= 0 )
			{
				System.out.println();
				System.err.print("You didn't make a right choice, try again: ");
			}

			else
				break;
		}

		System.out.println();

		if(myAnswer == rightAnswer){
			System.out.print("Congratulations! You have won " + GetFormatedNumbers.getFormatedNumber(bet.getYourBet() * 3) + "!");
			bank.update(Bank.WIN, bet.getYourBet() * 3);
			statistics.update(Statistics.WIN, bet.getYourBet() * 3);
			highscore.update();
		}


		if (myAnswer != rightAnswer){
			System.out.println("Too bad, that was not the right number, you lost your bet!");
			System.out.println("The right number was " + rightAnswer +".");
			bank.update(Bank.LOOSE, bet.getYourBet());
			statistics.update(Statistics.LOOSE, bet.getYourBet());
		}	
	}
}

