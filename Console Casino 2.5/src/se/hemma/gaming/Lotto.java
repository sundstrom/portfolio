package se.hemma.gaming;

import java.util.Arrays;
import java.util.Random;

public class Lotto {
	private Random dice = new Random();
	private Bank bank;
	private Bet bet;
	private Statistics statistics;
	private Highscore highscore;
	
	
	public void setAll(Bet bet, Statistics statistics, Bank bank, Highscore highscore){
		this.bet = bet;
		this.statistics = statistics;
		this.bank = bank;
		this.highscore = highscore;
	}

	//  GET A RANDOM NUMBER
	private int[] getRandomNumbers(int size)
	{
		int[] randomNumbers = new int[size];
		for (int i = 0; i < randomNumbers.length; i++) 
		{
			randomNumbers[i] = dice.nextInt(21);

			for (int j = 0; j < i; j++) 
			{
				if(randomNumbers[i] == randomNumbers[j])
				{
					i--;
					break;
				}
			}
		}
		Arrays.sort(randomNumbers);
		return randomNumbers;
	}

	public void play()
	{
		int[] randomNumbers = getRandomNumbers(5);
		int guess;


		System.out.print("5 numbers between 0-20 will be randomized, make a guess of a number that you think will show up: ");

		while(true)
		{

			Double myGuess = Number.TryParseDouble(); 
			guess = myGuess.intValue();

			if (guess < 0 || guess > 20)
				System.out.print("Invalid choice, try again: ");

			else
				break;		
		}

		int foundAt = Arrays.binarySearch(randomNumbers, guess);

		if(foundAt > -1)
		{
			System.out.println();				
			System.out.print("Congratulations! You have won " + GetFormatedNumbers.getFormatedNumber(bet.getYourBet() * 5) + "!");
			bank.update(Bank.WIN, bet.getYourBet() * 5);
			statistics.update(Statistics.WIN, bet.getYourBet() * 5);
			highscore.update();
		}

		else
		{
			System.out.println();
			System.out.println("Too bad, your number didn't show up, you lost your bet!");

			System.out.print("The random numbers where: ");
			for (int i: randomNumbers)
				System.out.print(i + " ");
			System.out.println();

			bank.update(Bank.LOOSE, 5);
			statistics.update(Statistics.LOOSE, bet.getYourBet());
		}


	}
}
