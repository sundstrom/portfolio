-------------------------------------------
CONSOLE CASINO

Console Casino �r ett projekt som jag arbetat med vid sidan av studierna.
Jag villa testa att arbeta med random funktioner, logik, matematik, samt skriva och l�sa fr�n filer.
S� det blev ett Casino i konsolf�nstret, d�rav namnet.

-------------------------------------------
EGET PROJEKT

Ifr�n starten av v�r utbildning har vi f�tt arbeta med v�rt egna projekt. Vi har f�tt v�lja ett valfritt programmeringsprojekt,
jag valde att g�ra ett spel utefter en id� som jag fick f�r m�nga �r sen.
D� vi haft mycket att l�ra under h�sten 2014 d� utbildningen startade, var det f�rst i b�rjan p� december 2014 som jag p� allvar kunde
komma ig�ng med mitt projekt.
Den tid vi hade tillg�nglig var fram till b�rjan p� jan. S� jag satt hela december och arbetade med projektet.

Jag har anv�nt mig av LibGDX och Box2d, b�da tv� var helt nya f�r mig och jag beh�vde l�ra mig dem fr�n grunden.
Jag valde dessa eftersom att mitt spel anv�nder sig av fysik, vilket Box2D st�der, och LibGDX kommer med Box2d.

Tanken med spelet �r att ta sig fr�n punkt A till punkt B, med hj�lp av gravitation och olika typer av f�rem�l, som p�verkar gravitationen.

-------------------------------------------
JBAY

Mer information finns i mappen "jbay", i pdf:en "Projektarbete".
Detta �r ett arbete som jag gjorde med 3 andra under en kurs d�r vi l�rde oss om databaser, s� det var st�rsta fokus, och inte GUI:t i sig.

De klasser jag ansvarat f�r och programmerat sj�lv �r Kund_GUI och Rapporter_GUI.
Dock har vi alla hj�lps �t och fels�kt �t varandra, arbetat fram metoder, skapat databasen och diagrammen tillsammans mm.

-------------------------------------------
HOMEPAGE

Det h�r �r en hemsida som inte p� n�got s�tt �r f�rdig �nnu, utan �r under utveckling. Arbetar mycket p� att g�ra designen responsiv genom att anv�nda Bootstrap. Ska �ven arbeta med jQuery.
Det �r en hemsida som jag t�nkt anv�nda f�r att sprida min musik.
Som den �r nu �r inte all engelska korrekt, men det ska ses �ver, descriptions f�r album ska uppdateras, och mer inneh�ll byggas p�. 

Detta �r ett projekt som jag har arbetat med p� sidan av. Fokus �n s� l�nge har varit p� att f� min Carousel och mina modals att fungera, samt den responsiva designen.